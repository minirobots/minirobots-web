// vim: expandtab ts=2 sw=2 ai

/**
 * Minirobots - Turtle Web Editor
 * https://minirobots.com.ar
 *
 * Author: Leo Vidarte <lvidarte@gmail.com>
 *
 * This is free software,
 * you can redistribute it and/or modify it
 * under the terms of the GPL version 3
 * as published by the Free Software Foundation.
 *
 */

'use strict';

/**
 * Create a namespace for the application.
 */
const Turtle = {};


Turtle.HEIGHT = 600;
Turtle.WIDTH  = 600;

/**
 * Commands to send to the robot.
 */
Turtle.commands = [];

/**
 * PID of animation task currently executing.
 */
Turtle.pid = 0;

/**
 * Should the turtle be drawn?
 */
Turtle.visible = true;

/**
 * Milliseconds to wait until next execution.
 */
Turtle.ms = 0;

/**
 * Current program.
 */
Turtle.program = {
  uuid: null,
  name: 'Mi programa',
  code: null,
  public: true,
  updatedAt: null,
};

/**
 * Max commands to send by request.
 */
Turtle.batchSize = 64;

/**
 * Robot status.
 */
Turtle.robot = {
  connected: false,
  queue: null
};

/**
 * Robot audio.
 */
Turtle.audio = {
  pid: 0,
  context: null,
  node: null,
};

/**
 * Create audio context.
 */
Turtle.createAudio = () => {
  Turtle.audio.pid = 0;
  Turtle.audio.context = new (window.AudioContext || window.webkitAudioContext)();
  Turtle.audio.node = Turtle.audio.context.createGain();
  Turtle.audio.node.connect(Turtle.audio.context.destination);
};

/**
 * Initialize Blockly and the turtle.  Called on page load.
 */
Turtle.init = () => {
  // document.dir fails in Mozilla, use document.body.parentNode.dir instead.
  // https://bugzilla.mozilla.org/show_bug.cgi?id=151407
  let rtl = document.body.parentNode.dir == 'rtl';
  let toolbox = document.getElementById('toolbox');
  Blockly.inject(document.getElementById('blockly'), {
     grid: {
      spacing : 20,
      length  : 2,
      colour  : '#ccc',
      snap    : true
    },
    zoom: {
      controls   : true,
      wheel      : false,
      startScale : 1.1,
      maxScale   : 5,
      minScale   : 1.0,
      scaleSpeed : 1.2
    },
    rtl      : rtl,
    toolbox  : toolbox,
    trashcan : true
  });

  Blockly.JavaScript.INFINITE_LOOP_TRAP = '  Blockly.Apps.checkTimeout(%1);\n';

  // Add to reserved word list: API, local variables in execution evironment
  // (execute) and the infinite loop detection function.
  Blockly.JavaScript.addReservedWords('Turtle,code');

  // Color override
  window.addEventListener('beforeunload', function(e) {
    const msg = 'Leaving this page will result in the loss of your work.';
    if (Blockly.mainWorkspace.getAllBlocks().length > 2) {
      e.returnValue = msg;  // Gecko.
      return msg;  // Webkit.
    }
    return null;
  });

  // Resize editor
  const blocklyDiv = document.getElementById('blockly');
  const navmenuDiv = document.getElementById('navmenu');
  const displayDiv = document.getElementById('display');
  const scratchDiv = document.getElementById('scratch');
  const canvasDiv = document.getElementById('canvas');
  const blocklyResize = (e) => {
    blocklyDiv.style.height = (window.innerHeight - navmenuDiv.getBoundingClientRect().height) + 'px';
    displayDiv.style.width = (canvasDiv.getBoundingClientRect().width - 0) + 'px';
    scratchDiv.style.width = (canvasDiv.getBoundingClientRect().width - 0) + 'px';
    Blockly.svgResize(Blockly.mainWorkspace);
  };
  window.addEventListener('resize', blocklyResize);
  blocklyResize();

  // Initlize display.
  Turtle.ctxDisplay = document.getElementById('display').getContext('2d');
  Turtle.ctxScratch = document.getElementById('scratch').getContext('2d');
  Turtle.reset();

  // Events
  document.getElementById('save-button').addEventListener('click', (event) => {
    document.getElementById('program-name').value = Turtle.program.name;
    document.getElementById('program-public').checked = Turtle.program.public;
    document.getElementById('program-image').src = document.getElementById('display').toDataURL('image/png');

    // UpdatedAt
    const textnode = document.createTextNode(Turtle.program.updatedAt);
    const item = document.getElementById('program-updated-at');
    if (item.childNodes[0] === undefined)
      item.appendChild(textnode);
    else
      item.replaceChild(textnode, item.childNodes[0]);
  });

  document.getElementById('program-save').addEventListener('click', (event) => {
    const programName = document.getElementById('program-name').value;
    const isPublic = document.getElementById('program-public').checked;
    const uuid = Turtle.program.uuid;
    const programImage = document.getElementById('program-image').src;
    Turtle.save(programName, programImage, isPublic, uuid);
  });

  document.getElementById('config-button').addEventListener('click', (event) => {
    document.getElementById('robot-ip').value = Turtle.getIP();
  });

  document.getElementById('config-save').addEventListener('click', (event) => {
    const ipOrCode = document.getElementById('robot-ip').value;
    if (ipOrCode === '') return;
    Turtle.setIP(ipOrCode);
  });

  //Turtle.checkRobotStatus();
};

/**
 * Minirobots API url.
 */
Turtle.getMinirobotsUrl = (path) => {
  return `${config.api_protocol}://${config.api_minirobots}/${path}`;
};

/**
 * Robot API url.
 */
Turtle.getRobotUrl = (path) => {
  return `${config.api_protocol}://${Turtle.getIP()}/${path}`;
};

/**
 * Check robot connection.
 */
Turtle.checkRobotStatus = () => {
  const url = Turtle.getRobotUrl('status');
  const request = new Request(url) 

  fetch(request)
    .then( response => {
      if (response.status === 200) return response.json();
      else throw new Error("Can't get the robot status");
    })
    .then( response => {
      if ( ! Turtle.robot.connected) window.alert('El Robot se ha conectado :)');
      document.getElementById('sendButton').disabled = false;
      document.getElementById('sendButton').title = 'Enviar programa al robot';
      Turtle.robot.connected = true;
      Turtle.robot.queue = response.queue;
      Turtle.setRobotButton( Turtle.robot.queue.left == Turtle.robot.queue.size ? 'send' : 'cancel' );
      console.log(JSON.stringify(Turtle.robot));
      window.setTimeout(Turtle.checkRobotStatus, 1000);
    })
    .catch( error => {
      if (Turtle.robot.connected) window.alert('El Robot se ha desconectado :(');
      document.getElementById('sendButton').disabled = true;
      document.getElementById('sendButton').title = 'No hay un robot conectado';
      Turtle.robot.connected = false;
      Turtle.robot.queue = null;
      console.log(JSON.stringify({ message: "Can't connect to robot" }));
      window.setTimeout(Turtle.checkRobotStatus, 1000);
    });
}

/**
 * Stop the turtle.
 */
Turtle.stop = () => {
  // Kill any task.
  if (Turtle.pid) window.clearTimeout(Turtle.pid);
  if (Turtle.audio.pid) window.clearTimeout(Turtle.audio.pid);
  Turtle.pid = 0;
  Turtle.audio.pid = 0;
};

/**
 * Reset the turtle to the start position, clear the display, and kill any
 * pending tasks.
 */
Turtle.reset = () => {
  // Starting location and heading of the turtle.
  Turtle.x = Turtle.HEIGHT / 2;
  Turtle.y = Turtle.WIDTH / 2;
  Turtle.heading = 0;
  Turtle.penDownValue = true;
  Turtle.visible = true;

  // Clear debugger
  document.getElementById("debugger").value = '';

  // Clear the display.
  Turtle.ctxScratch.canvas.width = Turtle.ctxScratch.canvas.width;
  Turtle.ctxScratch.strokeStyle = '#000000';
  Turtle.ctxScratch.lineWidth = 1;
  Turtle.ctxScratch.lineCap = 'round';
  Turtle.leds = { left: '', right: '' };
  Turtle.display();
  Turtle.stop()
};

/**
 * Copy the scratch canvas to the display canvas. Add a turtle marker.
 */
Turtle.display = () => {
  Turtle.ctxDisplay.globalCompositeOperation = 'copy';
  Turtle.ctxDisplay.drawImage(Turtle.ctxScratch.canvas, 0, 0);
  Turtle.ctxDisplay.globalCompositeOperation = 'source-over';

  if (Turtle.visible) {
    const penColor    = '#cccccc';
    const turtleColor = '#2196F3';

    // Draw center of turtle.
    Turtle.ctxDisplay.beginPath();
    Turtle.ctxDisplay.arc(Turtle.x, Turtle.y, 2, 0, 2 * Math.PI, false);
    Turtle.ctxDisplay.lineWidth = 3;
    Turtle.ctxDisplay.strokeStyle = turtleColor;
    Turtle.ctxDisplay.stroke();

    // Draw the turtle body.
    let radius = Turtle.ctxScratch.lineWidth / 2 + 20;
    Turtle.ctxDisplay.beginPath();
    Turtle.ctxDisplay.arc(Turtle.x, Turtle.y, radius, 0, 2 * Math.PI, false);
    Turtle.ctxDisplay.lineWidth = 3;
    Turtle.ctxDisplay.strokeStyle = penColor;
    Turtle.ctxDisplay.stroke();

    // Drqw the turtle leds.
    if (Turtle.leds.left) {
      const radiansLeft = 2 * Math.PI * (Turtle.heading - 35) / 360;
      const ledLeftX = Turtle.x + (radius - 2) * Math.sin(radiansLeft);
      const ledLeftY = Turtle.y - (radius - 2) * Math.cos(radiansLeft);
      Turtle.ctxDisplay.fillStyle = Turtle.leds.left;
      Turtle.ctxDisplay.fillRect(ledLeftX - 2, ledLeftY - 2, 4, 4);
    }
    if (Turtle.leds.right) {
      const radiansRight = 2 * Math.PI * (Turtle.heading + 35) / 360;
      const ledRightX = Turtle.x + (radius - 2) * Math.sin(radiansRight);
      const ledRightY = Turtle.y - (radius - 2) * Math.cos(radiansRight);
      Turtle.ctxDisplay.fillStyle = Turtle.leds.right;
      Turtle.ctxDisplay.fillRect(ledRightX - 2, ledRightY - 2, 4, 4);
    }

    // Draw the turtle head.
    radius -= 17;
    const WIDTH = 1.3;
    const HEAD_TIP = 8;
    const ARROW_TIP = 4;
    const BEND = 5;
    let radians = 2 * Math.PI * Turtle.heading / 360;
    const tipX = Turtle.x + (radius + HEAD_TIP) * Math.sin(radians);
    const tipY = Turtle.y - (radius + HEAD_TIP) * Math.cos(radians);
    radians -= WIDTH;
    const leftX = Turtle.x + (radius + ARROW_TIP) * Math.sin(radians);
    const leftY = Turtle.y - (radius + ARROW_TIP) * Math.cos(radians);
    radians += WIDTH / 2;
    const leftControlX = Turtle.x + (radius + BEND) * Math.sin(radians);
    const leftControlY = Turtle.y - (radius + BEND) * Math.cos(radians);
    radians += WIDTH;
    const rightControlX = Turtle.x + (radius + BEND) * Math.sin(radians);
    const rightControlY = Turtle.y - (radius + BEND) * Math.cos(radians);
    radians += WIDTH / 2;
    const rightX = Turtle.x + (radius + ARROW_TIP) * Math.sin(radians);
    const rightY = Turtle.y - (radius + ARROW_TIP) * Math.cos(radians);
    Turtle.ctxDisplay.beginPath();
    Turtle.ctxDisplay.fillStyle = penColor;
    Turtle.ctxDisplay.moveTo(tipX, tipY);
    Turtle.ctxDisplay.lineTo(leftX, leftY);
    Turtle.ctxDisplay.bezierCurveTo(
      leftControlX, leftControlY,
      rightControlX, rightControlY,
      rightX, rightY
    );
    Turtle.ctxDisplay.closePath();
    Turtle.ctxDisplay.fill();
  }

};

/**
 * Click the run button. Start the program.
 */
Turtle.runButtonClick = () => {
  Blockly.mainWorkspace.highlightBlock(null);
  Turtle.createAudio();
  Turtle.execute();
};

/**
 * Click the stop button. Stop the Turtle.
 */
Turtle.stopButtonClick = () => {
  Turtle.setDrawButton('clear');
  Turtle.stop();
  Blockly.mainWorkspace.highlightBlock(null);
};

/**
 * Click the reset button. Reset the Turtle.
 */
Turtle.clearButtonClick = () => {
  Turtle.setDrawButton('run');
  Turtle.reset();
  Blockly.mainWorkspace.highlightBlock(null);
};

/**
 * Click the send button. Send commands to Robot.
 */
Turtle.sendButtonClick = () => {
  Blockly.mainWorkspace.highlightBlock(null);
  window.setTimeout(Turtle.send, 100);
};

/**
 * Abort robot execution program and clear queue.
 */
Turtle.cancelButtonClick = () => {
  Turtle.commands = [];
  const url = Turtle.getRobotUrl('program');
  const method = 'POST';
  const body = JSON.stringify({'CMD': [['QE', 'clear']]});
  const request = new Request(url, { method, body });

  fetch(request)
    .then( response => {
      window.alert('Ejecución cancelada !');
    })
    .catch( error => {
      console.error(error);
    });
};

/**
 * Execute the user's code.  Heaven help us...
 */
Turtle.execute = () => {
  Blockly.Apps.log = [];
  Blockly.Apps.ticks = 1000000;
  Turtle.commands = [];

  const code = Blockly.JavaScript.workspaceToCode(Blockly.mainWorkspace);
  try {
    console.log(code);
    eval(code);
  }
  catch (e) {
    // Null is thrown for infinite loop.
    // Otherwise, abnormal termination is a user error.
    if (e !== null) {
      alert(e);
    }
  }

  Turtle.reset();
  Turtle.setDrawButton('stop');
  Turtle.pid = window.setTimeout(Turtle.animate, 100);
};

/**
 * Get the "real" commands length.
 */
Turtle.commandsLength = () => {
  let length = 0;
  for (let i = 0; i < Turtle.commands.length; i++) {
    if (Turtle.commands[i][0] == "LD") length += 4;
    else if (Turtle.commands[i][0] == "TE") length += 2;
    else length++;
  }
  return length;
};

/**
 * Get commands based on the real commands length.
 */
Turtle.getCommands = (length) => {
  let size = 0;
  let i = 0;
  for ( ; i < Turtle.commands.length; i++) {
    if (Turtle.commands[i][0] == "LD") {
      if (size + 4 <= length) size += 4;
      else break;
    }
    else if (Turtle.commands[i][0] == "TE") {
      if (size + 2 <= length) size += 2;
      else break;
    }
    else {
      if (size + 1 <= length) size += 1;
      else break;
    }
  }
  return Turtle.commands.splice(0, i);
};

/**
 * Send commands to the robot.
 */
Turtle.send = () => {
  Blockly.Apps.log = [];
  Turtle.commands = [];

  var code = Blockly.JavaScript.workspaceToCode(Blockly.mainWorkspace);
  try {
    eval(code);
  }
  catch (e) {
    // Null is thrown for infinite loop.
    // Otherwise, abnormal termination is a user error.
    if (e !== null) {
      alert(e);
    }
  }

  if (Turtle.commands.length) {
    Turtle.commands.unshift(['QE', 'clear']);
    console.log(JSON.stringify({ program: Turtle.commands }));
    Turtle.sendCommands();
  }
  else {
    window.alert('No hay instrucciones para enviar a la tortuga !');
    return;
  }
}

/**
 * Send commands to the robot.
 */
Turtle.sendCommands = () => {
  if ( ! Turtle.robot.connected) {
    window.alert('No hay conexión con el robot :(');
    return;
  }
  if ( ! Turtle.commands.length) {
    return;
  }

  const commandsLength = Turtle.commandsLength();
  const length = (commandsLength > Turtle.batchSize) ? Turtle.batchSize : commandsLength;

  if (Turtle.robot.queue.left > length) {
    const cmd = Turtle.getCommands(length);
    const url = Turtle.getRobotUrl('program');
    const method = 'POST';
    const body = JSON.stringify({CMD: cmd});
    const request = new Request(url, { method, body });
    console.log(JSON.stringify({ body }));

    // Send program
    fetch(request)
      .then( response => {
        if (response.status === 201) return response.json();
        else throw new Error('Error enviando programa al robot');
      })
      .then( response => {
        console.log(JSON.stringify(response));
        if (Turtle.commands.length) {
          console.log(JSON.stringify({ send: 'Adding call to sendCommands...' }));
          window.setTimeout(Turtle.sendCommands, 1000);
        }
        else {
          console.log(JSON.stringify({ send: 'Program sent !' }));
        }
      })
      .catch( error => {
        console.error(error);
        window.alert(error);
      });
  }
  else {
    console.log(JSON.stringify({ send: 'Adding call to sendCommands (queue full)...' }));
    window.setTimeout(Turtle.sendCommands, 1000);
  }

};

/**
 * Show the user's code in raw JavaScript.
 */
Turtle.showCode = () => {
  var code = Blockly.JavaScript.workspaceToCode();
  // Strip out serial numbers.
  code = code.replace(/(,\s*)?'\d+'\)/g, ')');
  alert(code);
};

/**
 * Get the robot's IP.
 */
Turtle.getIP = () => {
  var ip = localStorage.getItem('ip');
  if ( ip == null || ip == 'null' ) { ip = config.ip; }
  return ip;
};

/**
 * Set the robot's IP.
 */
Turtle.setIP = (ipOrCode) => {
  // Name! find last ip in api
  if (ipOrCode.length === 6) {
    const code = ipOrCode.toLowerCase();
    const re = /[0-9a-f]{6}/g;
    if (re.test(code)) {
      const url = Turtle.getMinirobotsUrl(`robots/${code}/ip`);
      const request = new Request(url)
      fetch(request)
        .then( response => {
          if (response.status === 200) return response.json();
          else if (response.status === 404) throw new Error("Robot no encontrado");
          else throw new Error("Error obteniendo la IP del robot");
        })
        .then( response => {
          window.alert(`La IP del robot es ${response.robot.ip}\nRegistrada el ${response.robot.created}`);
          localStorage.setItem('ip', response.robot.ip);
        })
        .catch( error => {
          window.alert(error.message);
          console.log(JSON.stringify({ message: error.message }));
        });
    }
    else {
      window.alert("El código ingresado no es correcto");
    }
  }
  // IP to local storage
  else {
    const ip = ipOrCode;
    const re = /^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$/;
    if (re.test(ip)) {
      localStorage.setItem('ip', ip);
    }
    else {
      window.alert("La IP ingresada no es válida");
    }
  }
};

/**
 * Set visibility for the current canvas draw button [run, stop, clear]
 */
Turtle.setDrawButton = (action) => {
  document.getElementById('runButton').style.display   = (action === 'run'  ) ? 'inline' : 'none';
  document.getElementById('stopButton').style.display  = (action === 'stop' ) ? 'inline' : 'none';
  document.getElementById('clearButton').style.display = (action === 'clear') ? 'inline' : 'none';
  document.getElementById('spinner').style.visibility  = (action === 'stop' ) ? 'visible': 'hidden';
};

/**
 * Set visibility for the current robot button [send, cancel]
 */
Turtle.setRobotButton = (action) => {
  document.getElementById('sendButton').style.display   = (action === 'send'  ) ? 'inline' : 'none';
  document.getElementById('cancelButton').style.display = (action === 'cancel') ? 'inline' : 'none';
  document.getElementById('spinner').style.visibility   = (action !== 'send'  ) ? 'visible': 'hidden';
};

/**
 * Set workspace from text file.
 */
Turtle.setWorkspace = (program) => {
  Turtle.program = program;
  var xml = Blockly.Xml.textToDom(program.code);
  Blockly.mainWorkspace.clear();
  Blockly.Xml.domToWorkspace(xml, Blockly.mainWorkspace);
};

/**
 * Load workspace from .minirobots file.
 */
Turtle.loadFile = () => {
  if ( !window.File || !window.FileReader || !window.FileList || !window.Blob ) {
    window.alert('Tu navegador no soporta la opción importar :(');
    return;
  }
  document.getElementById('my_file').addEventListener('change', Turtle.handleFileSelect, false);
  document.getElementById('my_file').click();
};

Turtle.handleFileSelect = (event) => {
  const files  = event.target.files; // FileList object
  const reader = new FileReader();

  reader.onload = ( reader => {
    return () => {
      const program = reader.result.replace(/\r?\n|\r|\n/g, '').trim();
      const name = reader.fileName;
      if (program.startsWith('<xml xmlns="http://www.w3.org/1999/xhtml">') && program.endsWith('</xml>')) {
        Turtle.setWorkspace(program, name);
      }
      else {
        window.alert('El formato del archivo no es válido :(');
      }
    }
  })(reader);

  for (let i = 0, f; f = files[i]; i++) {
    if (f.name.endsWith('.minirobots')) {
      reader.fileName = f.name.replace(/\.minirobots$/, '');
      reader.readAsText(f);
    }
  }
};

/**
 * Save workspace to DB
 */
Turtle.save = (programName, programImage, isPublic, uuid) => {
  const xml  = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
  const text = Blockly.Xml.domToText(xml);

  const csrftoken = document.querySelector("#csrf input").value;

  const data = {
    uuid: uuid,
    name: programName,
    image: programImage,
    code: text,
    public: isPublic,
  }
  console.log('data', data);

  fetch('/editor/ajax/program', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-CSRFToken': csrftoken,
    },
    body: JSON.stringify(data),
  })
  .then(response => response.json())
  .then(data => {
    Turtle.program.uuid = data.uuid;
    Turtle.program.name = data.name;
    Turtle.program.public = data.public;
    Turtle.program.updatedAt = data.updated_at;
    console.error('Success', data);
  })
  .catch((error) => {
    console.error('Error', error);
  });

};

/**
 * Play tone.
 * https://7tonshark.com/2018-09-16-web-audio-part-1
 */
Turtle.tone = (freq, ms) => {
  return new Promise( (resolve, reject) => {
    const context = Turtle.audio.context;
    const node = Turtle.audio.node;

    const o = node.context.createOscillator();
    const start = context.currentTime;
    const length = ms / 1000;

    o.connect(node);
    o.type = 'square'; // sine, square, sawtooth, triangle
    o.frequency.value = freq;

    // At note=0%, volume should be 0%
    node.gain.setValueAtTime(0, start);

    // Over the first 10% of the note, ramp up to 100% volume
    node.gain.linearRampToValueAtTime(1, start + length * 0.1);

    // Keep it at 100% volume up until 90% of the note's length
    node.gain.setValueAtTime(1, start + length * 0.9);

    // By 99% of the note's length, ramp down to 0% volume
    node.gain.linearRampToValueAtTime(0, start + length * 0.99);

    o.start();

    Turtle.audio.pid = window.setTimeout( () => {
      o.stop();
      resolve();
    }, ms);
  });
};

/**
 * Iterate through the recorded path and animate the turtle's actions.
 */
Turtle.animate = async () => {
  // All tasks should be complete now. Clean up the PID list.
  Turtle.pid = 0;
  Turtle.audio.pid = 0;

  // Weird! The sleep has to be put here, after the animate execution,
  // in order to highlight correctly the block.
  if (Turtle.ms) {
    const start = new Date().getTime();
    for (let i = 0; i < 1e7; i++)
      if ((new Date().getTime() - start) > Turtle.ms)
        break;
    Turtle.ms = 0;
  }

  const tuple = Blockly.Apps.log.shift();
  if (!tuple) {
    document.getElementById('spinner').style.visibility = 'hidden';
    document.getElementById('stopButton').style.display = 'none';
    document.getElementById('clearButton').style.display = 'inline';
    Blockly.mainWorkspace.highlightBlock(null);
    return;
  }
  Blockly.mainWorkspace.highlightBlock(tuple.pop());

  const decimalToHexString = (n) => {
    const hex = Number(n).toString(16);
    return (hex.length < 2) ? "0" + hex : hex;
  };

  const rgbToHexString = (r, g, b) => {
    return `#${decimalToHexString(r)}${decimalToHexString(g)}${decimalToHexString(b)}`;
  };

  switch (tuple[0]) {
    case 'FD':  // Forward
      if (Turtle.penDownValue) {
        Turtle.ctxScratch.beginPath();
        Turtle.ctxScratch.moveTo(Turtle.x, Turtle.y);
      }
      const distance = tuple[1];
      let bump = 0;
      if (distance) {
        Turtle.x += distance * Math.sin(2 * Math.PI * Turtle.heading / 360);
        Turtle.y -= distance * Math.cos(2 * Math.PI * Turtle.heading / 360);
        bump = 0;
      } else {
        // WebKit (unlike Gecko) draws nothing for a zero-length line.
        bump = 0.1;
      }
      if (Turtle.penDownValue) {
        Turtle.ctxScratch.lineTo(Turtle.x, Turtle.y + bump);
        Turtle.ctxScratch.stroke();
      }
      break;
    case 'RT':  // Right Turn
      Turtle.heading += tuple[1];
      Turtle.heading %= 360;
      if (Turtle.heading < 0) {
        Turtle.heading += 360;
      }
      break;
    case 'PU':  // Pen Up
      Turtle.penDownValue = false;
      break;
    case 'PD':  // Pen Down
      Turtle.penDownValue = true;
      break;
    case 'SP':  // Sleep
      Turtle.ms = tuple[1];
      break;
    case 'PW':  // Pen Width
      Turtle.ctxScratch.lineWidth = tuple[1];
      break;
    case 'PC':  // Pen Color
      Turtle.ctxScratch.strokeStyle = tuple[1];
      break;
    case 'HT':  // Hide Turtle
      Turtle.visible = false;
      break;
    case 'ST':  // Show Turtle
      Turtle.visible = true;
      break;
    case 'LD':  // Handle leds
      const led = tuple[1][0];
      const r = tuple[1][1];
      const g = tuple[1][2];
      const b = tuple[1][3];
      let color = rgbToHexString(r, g, b);
      if (color == '#000000') color = '';
      if (led == 0 || led == 2) Turtle.leds.right = color;
      if (led == 1 || led == 2) Turtle.leds.left = color;
      break;
    case 'TE':  // Handle tones
      const freq = tuple[1][0];
      const ms = tuple[1][1];
      await Turtle.tone(freq, ms);
      break;
    case 'DEBUGGER':
      document.getElementById("debugger").value += tuple[1] + '\n';
      break;
  }
  Turtle.display();

  const stepSpeed = 1000 - document.getElementById("speed").value;
  Turtle.pid = window.setTimeout(Turtle.animate, stepSpeed);
};

// Turtle API.

Turtle.moveForward = (distance, id) => {
  Turtle.commands.push(['FD', distance]);
  Blockly.Apps.log.push(['FD', distance, id]);
};

Turtle.moveBackward = (distance, id) => {
  Turtle.commands.push(['BD', distance]);
  Blockly.Apps.log.push(['FD', -distance, id]);
};

Turtle.turnRight = (angle, id) => {
  Turtle.commands.push(['RT', angle]);
  Blockly.Apps.log.push(['RT', angle, id]);
};

Turtle.turnLeft = (angle, id) => {
  Turtle.commands.push(['LT', angle]);
  Blockly.Apps.log.push(['RT', -angle, id]);
};

Turtle.penUp = (id) => {
  Turtle.commands.push(['PN', 0]);
  Blockly.Apps.log.push(['PU', id]);
};

Turtle.penDown = (id) => {
  Turtle.commands.push(['PN', 1]);
  Blockly.Apps.log.push(['PD', id]);
};

Turtle.ledColour = (led, hex_colour, id) => {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex_colour);
  const r = parseInt(result[1], 16);
  const g = parseInt(result[2], 16);
  const b = parseInt(result[3], 16);
  Turtle.commands.push(['LD', [led, r, g, b]]);
  Blockly.Apps.log.push(['LD', [led, r, g, b], id]);
};

Turtle.playTone = (tone, sec, id) => {
  Turtle.commands.push(['TE', [tone, sec * 1000]]);
  Blockly.Apps.log.push(['TE', [tone, sec * 1000], id]);
};

Turtle.sleep = (sec, id) => {
  Turtle.commands.push(['SP', sec * 1000]);
  Blockly.Apps.log.push(['SP', sec * 1000, id]);
};

Turtle.debugger = (msg, id) => {
  Blockly.Apps.log.push(['DEBUGGER', msg.toString(), id]);
};
